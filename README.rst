=====
Redwood Labs Core
=====

This package contains templates, themes, and logic used for Redwood Labs projects.

Quick start
-----------

1. Add "redwoodlabs" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'redwoodlabs',
    ]

2. Add "redwoodlabs.urls" to your project's `urls.py`::

    path('', include('redwoodlabs.urls')),
