<div class="form-control" style="height:auto;">
  <div class="s3direct" style="float:none; overflow: hidden;" data-policy-url="{{ policy_url }}" data-signing-url="{{ signing_url }}">
    <div id="image-preview"><img class="file-image" src="{{ file_url }}" style="max-width: 100%; max-height: 400px;"></div>
    <a class="file-link btn btn-styled btn-rounded btn-link p-0 btn-icon-right mt-2" target="_blank" href="{{ file_url }}">Open<i class="fa fa-external-link"></i></a>
    <a class="file-remove btn btn-sm btn-rounded btn-base-3 btn-outline btn-icon-left text-capitalize mt-2 mb-1" href="#remove" onClick="$('#image-preview').remove();">
      <i class="fa fa-trash-o"></i>Remove
    </a>
    <input class="csrf-cookie-name" type="hidden" value="{{ csrf_cookie_name }}">
    <input class="file-url" type="hidden" value="{{ file_url }}" id="{{ element_id }}" name="{{ name }}" />
    <input class="file-dest" type="hidden" value="{{ dest }}">
    <input class="file-input" type="file"  style="{{ style }}"/>
    <div class="progress progress-striped progress-bar-animated active rounded">
      <div class="bar"></div>
      <a href="#cancel" class="cancel-button">&times;</a>
    </div>
  </div>
</div>