{% load account %}{% user_display user as user_display %}{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain name=user.first_name %}{{name}},

Thanks for trying {{ site_name }}! It's an early version, and we'd appreciate all the feedback you're willing to give.

Confirm your email address here: {{ activate_url }}

{% endblocktrans %}{% endautoescape %}
{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Thanks,

Brenden from {{ site_name }}
https://twitter.com/mulligan

{% endblocktrans %}