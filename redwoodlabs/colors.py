from PIL import Image
import urllib.request
import io
import ssl
import copy

from colorthief import ColorThief

def create_image_object(image_path):
    if hasattr(ssl, '_create_unverified_context'):
        ssl._create_default_https_context = ssl._create_unverified_context
    with urllib.request.urlopen(image_path) as url:
        image = io.BytesIO(url.read())
    return image

def rgb_to_hex(rgb):
    return '#%02x%02x%02x' % rgb

def hex_to_rgb(hex, opacity=1.0):
    if '#' in hex:
        hex = hex.replace('#','')
    try:
        rgb = ','.join(tuple(str(int(hex[i:i+2], 16)) for i in (0, 2, 4))) + ',%s' % opacity
    except:
        rgb = '0,0,0,%s' % opacity
    return rgb

def get_color_from_image(image_path, quality=1, format='hex'):
    color_thief = ColorThief(create_image_object(image_path))
    dominant_color = color_thief.get_color(quality=quality)
    if format == 'hex':
        dominant_color = rgb_to_hex(dominant_color)
    return dominant_color

def get_color_palette_from_image(image_path, quantity=6, format='hex'):
    color_thief = ColorThief(create_image_object(image_path))
    palette = color_thief.get_palette(color_count=quantity)

    if format == 'hex':
        rgb_colors = copy.deepcopy(palette)
        palette = []
        for color in rgb_colors:
            palette.append(rgb_to_hex(color))

    return palette