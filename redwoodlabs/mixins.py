class FilterQuerySetByUserMixin:
    user_field = 'user'

    def get_queryset(self):
        if not self.request.user.is_authenticated or self.request.user.is_superuser:
            return super().get_queryset()
        else:
            return super().get_queryset().filter(
                **{self.user_field: self.request.user}
            )
