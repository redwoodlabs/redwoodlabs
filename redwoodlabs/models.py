from django.db import models
from django.contrib.auth.models import User

from s3direct.fields import S3DirectField
from django.contrib.postgres.fields import JSONField

class UserProfile(models.Model):

    user = models.OneToOneField(User,
        on_delete=models.CASCADE,
        primary_key=True)

    profile_photo = S3DirectField(dest='images', null=True, blank=True)
    bio = models.TextField(max_length=280, null=True, blank=True)
    website = models.URLField(max_length=280, null=True, blank=True)
    twitter_username = models.CharField(max_length=280, null=True, blank=True)

    timezone = models.CharField(max_length=300, null=True, blank=True)
    last_notified = models.DateTimeField(null=True, blank=True)

    flags = JSONField(null=True, blank=True)

    unsubscribe = models.BooleanField(default=False)

    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username
