from django.contrib import admin

from redwoodlabs.models import *

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'create_time', )
admin.site.register(UserProfile, UserProfileAdmin)
