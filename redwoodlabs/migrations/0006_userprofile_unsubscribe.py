# Generated by Django 3.0.2 on 2020-01-22 17:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('redwoodlabs', '0005_auto_20200120_2229'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='unsubscribe',
            field=models.BooleanField(default=False),
        ),
    ]
