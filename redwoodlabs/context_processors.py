from django.conf import settings
from django.core.cache import cache
from django.shortcuts import get_object_or_404

from identity.strings import GLOBAL_STRINGS

DEFAULT_CACHE_TIME = 60 * 5

def global_strings(request):
    return {
        'STRINGS': GLOBAL_STRINGS,
        'DEBUG': settings.DEBUG,
    }

def global_variables(request):
    variables = {}
    '''
    if request.user.is_authenticated:
        try:
            resume = get_object_or_404(Resume, user=request.user)
        except:
            resume = None

        cover_letters = CoverLetter.objects.filter(resume=resume)

        variables = {
            'resume': resume,
            'cover_letters': cover_letters,
        }
    '''
    return variables
