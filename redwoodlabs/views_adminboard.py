from django.contrib.auth.models import User
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import View, DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator

try:
    from dashboard.admin import ADMINBOARD_STATS
except ImportError:
    ADMINBOARD_STATS = {'models': [],}

try:
    from dashboard.admin import ADMINBOARD_PREVIEW
except ImportError:
    ADMINBOARD_PREVIEW = {'model': None,}

if 'scale' in ADMINBOARD_PREVIEW:
    ADMINBOARD_PREVIEW['scale_percentage'] = 100 / ADMINBOARD_PREVIEW['scale']
else:
    ADMINBOARD_PREVIEW['scale_percentage'] = 100/.75

if not 'preview_height' in ADMINBOARD_PREVIEW:
    ADMINBOARD_PREVIEW['preview_height'] = 600

ADMINBOARD_STATS['models'].insert(0, User)

@method_decorator(staff_member_required, name='dispatch')
class CreatedObjectPreviewPage(LoginRequiredMixin, ListView):
    model = ADMINBOARD_PREVIEW['model']
    template_name = 'adminboard/preview_page.html'
    extra_context = {
        'preview_config': ADMINBOARD_PREVIEW,
    }

@method_decorator(staff_member_required, name='dispatch')
class StatsPage(LoginRequiredMixin, View):
    template_name = 'adminboard/stats_page.html'
    def get(self, request):

        stats = []

        for model in ADMINBOARD_STATS['models']:
            stats.append({
                'label': model._meta.verbose_name_plural.title(),
                'count': model.objects.all().count(),
            })

        if 'other' in ADMINBOARD_STATS:
            stats += ADMINBOARD_STATS['other']

        context = {
            'stats': stats,
        }
        return render(request, self.template_name, context)

