# Helper for models to create longer, non guessable random primary keys

# In models.py, add:
# from redwoodlabs.helpers import generate_random_id
# And to any model, add:
# id = models.BigIntegerField(default=generate_random_id, primary_key=True)


import time
import random
START_TIME = 343897500
def generate_random_id():
    # inspired by http://instagram-engineering.tumblr.com/post/10853187575/sharding-ids-at-instagram
    t = int(time.time()*1000) - START_TIME
    u = random.SystemRandom().getrandbits(3)
    id = (t << 3 ) | u
    return id

def reverse_random_id(id):
    t  = id >> 3
    return t + START_TIME
