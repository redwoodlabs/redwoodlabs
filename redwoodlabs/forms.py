from datetime import date

from django import forms
from django.forms.widgets import HiddenInput
from django.forms import ModelForm
from django.db.models import Q
from s3direct.widgets import S3DirectWidget
from django.core import validators

from django.contrib.auth.models import User

from redwoodlabs.models import UserProfile

class UserProfileForm(forms.ModelForm):
    username = forms.SlugField(required=True)
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    profile_photo = forms.URLField(widget=S3DirectWidget(dest='images'), required=False)
    bio = forms.CharField(widget=forms.Textarea, required=False, max_length=280)
    website = forms.URLField(required=False)
    twitter_username = forms.SlugField(required=False, max_length=100)

    class Meta:
        model = UserProfile
        fields = ('username', 'email', 'first_name', 'last_name', 'profile_photo', 'bio', 'website', 'twitter_username')

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if email and User.objects.filter(email=email).exclude(pk=self.instance.user.pk).count():
            raise forms.ValidationError('This email address is already in use. Please supply a different email address.')
        return email

    def clean_username(self):
        username = self.cleaned_data.get('username')

        if username and User.objects.filter(username=username).exclude(pk=self.instance.user.pk).count():
            raise forms.ValidationError('This username is already in use. Please supply a different username.')
        return username

    def save(self, commit=True):
        profile = super(UserProfileForm, self).save(commit=False)
        profile.user.email = self.cleaned_data['email']
        profile.user.username = self.cleaned_data['username']
        profile.user.first_name = self.cleaned_data['first_name']
        profile.user.last_name = self.cleaned_data['last_name']
        profile.user.bio = self.cleaned_data['bio']
        profile.user.twitter_username = self.cleaned_data['twitter_username']
        profile.user.website = self.cleaned_data['website']

        if commit:
            profile.user.save()
            profile.save()

        return profile


class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=30, label='First Name')
    last_name = forms.CharField(max_length=30, label='Last Name')

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()

