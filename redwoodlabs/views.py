from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from django import forms
from django.views.defaults import page_not_found, server_error

from redwoodlabs.models import UserProfile
from redwoodlabs.forms import UserProfileForm

class ProfileView(LoginRequiredMixin, View):
    model = UserProfile
    template_name = 'account/update_profile.html'

    def get_object(self):
        profile, created = UserProfile.objects.get_or_create(user=self.request.user)
        return profile

    def get_context_data(self, request):
        profile = self.get_object()
        initial = {
            'username': profile.user.username,
            'email': profile.user.email,
            'first_name': profile.user.first_name,
            'last_name': profile.user.last_name,
            'timezone': profile.timezone,
        }
        context = {
            'form': UserProfileForm(instance=profile, initial=initial),
            'form_title': 'Edit Profile'
        }
        return context

    def get(self, request):
        context = self.get_context_data(request)
        return render(request, self.template_name, context)

    def post(self, request):
        profile = self.get_object()
        context = self.get_context_data(request)
        form = UserProfileForm(request.POST, instance=profile)

        if form.is_valid():
            form.save()
            return redirect('update_profile')

        else:
            context['form'] = form

        return render(request, self.template_name, context)



def handler404(request, exception):
    return page_not_found(request, exception, template_name="404.html")

def handler500(request):
    return server_error(request, template_name='500.html')
