from django.urls import path
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView
from django.conf import settings

from . import views, views_adminboard

urlpatterns = [

    path('login/', RedirectView.as_view(pattern_name='account_login', permanent=True)),
    path('logout/', RedirectView.as_view(pattern_name='account_logout', permanent=True)),
    path('signup/', RedirectView.as_view(pattern_name='account_signup', permanent=True)),

    path('privacy/', TemplateView.as_view(template_name='marketing/privacy.html'), name='privacy_page'),
    path('terms/', TemplateView.as_view(template_name='marketing/terms.html'), name='terms_page'),

    path('robots\.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    path('favicon.ico', RedirectView.as_view(url=settings.STATIC_URL + 'identity/images/favicon.png', permanent=True)),

    path('account/profile/', views.ProfileView.as_view(), name='update_profile'),

    path('adminboard/', views_adminboard.StatsPage.as_view(), name='adminboard_stats_page'),
    path('adminboard/preview/', views_adminboard.CreatedObjectPreviewPage.as_view(), name='adminboard_preview_page'),

]
