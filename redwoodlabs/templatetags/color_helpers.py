from django import template
register = template.Library()

from redwoodlabs.colors import rgb_to_hex as rgb_to_hex_converter
from redwoodlabs.colors import hex_to_rgb as hex_to_rgb_converter

@register.filter
def hex_to_rgb(hex, opacity=1.0):
    return hex_to_rgb_converter(hex, opacity)

@register.filter
def rgb_to_hex(rgb):
    return rgb_to_hex_converter(rgb)

