from django.apps import AppConfig


class RedwoodCoreConfig(AppConfig):
    name = 'redwoodlabs'

    def ready(self):
        import redwoodlabs.signals
