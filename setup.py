import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='redwoodlabs',
    version='0.3',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',  # example license
    long_description=README,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: X.Y',  # replace "X.Y" as appropriate
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',  # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'django-registration==2.2',
        'django-allauth==0.40.0',
        'django-bootstrap4==1.1.1',
        'bootstrap-admin==0.4.3',
        'sentry-sdk==0.19.4',
        'django-s3direct==1.1.5',
        'sendgrid-django==4.2.0',
        'django-bootstrap-datepicker-plus==3.0.5',
        'django-ordered-model==3.4.1',
        'django-multiselectfield==0.1.11',
        'django-premailer==0.2.0',
        'django-debug-toolbar==2.1',
        'dj-database-url==0.5.0',
        'dj-static==0.0.6',
        'gunicorn==20.0.4',
        'psycopg2-binary==2.8.4',
        'static3==0.7.0',
        'pylibmc==1.6.1',
        'django-pylibmc==0.6.1',
        'django-import-export==2.0.1',
        'django-gravatar2==1.4.2',
        'django-mathfilters==0.4.0',
        'colorthief==0.2.1',
        'django-colorfield==0.3.2',
    ]
)



